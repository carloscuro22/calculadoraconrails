require "test_helper"

class InicioControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get inicio_new_url
    assert_response :success
  end

  test "should get create" do
    get inicio_create_url
    assert_response :success
  end
end
