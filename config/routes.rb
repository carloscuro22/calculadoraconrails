Rails.application.routes.draw do
  get 'inicio/new'
  get 'inicio/create'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "inicio#index"

  #Aqui definimos las APIS a consumir
  post 'logica', to: 'inicio#multxmont'
  post 'logxmes', to: 'inicio#logxmes'
end
