class InicioController < ApplicationController
  def index
  end

  def multxmont
    monto = params[:monto].to_f
    prec_btc = params[:precbtc].to_f
    prec_eth = params[:preceth].to_f

    total_btc = prec_btc*monto
    total_eth = prec_eth*monto 
    
    @retornresp = { :total_btc => total_btc, :total_eth => total_eth}

    render json: @retornresp
  end

  def logxmes
    meses = ["ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SETIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE"]
    
    totbtc = params[:totbtc].to_f
    toteth = params[:toteth].to_f
    
    #Para BTC
    @total_btc = []
    sum_btc = 0
    meses.each do |i,b|
      montmesbtc = 0.05*totbtc
      sum_btc += montmesbtc
      l = [
        :mes => i,
        :total => sum_btc]
      @total_btc << l
    end

    #Para ETH
    @total_eth = []
    sum_eth = 0
    meses.each do |a,e|
      montmeseth = 0.03*toteth
      sum_eth += montmeseth
      et = [
        :mes => a,
        :total => sum_eth]
      @total_eth << et
    end

    @retornrespuestames = { :totalbtc => @total_btc, :totaleth => @total_eth}

    render json: @retornrespuestames
  end

end
